package API.Repository;

import API.Tables.Offre;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@EnableJpaRepositories
@Repository
public interface OffreRepository extends CrudRepository<Offre, Long> {
}
