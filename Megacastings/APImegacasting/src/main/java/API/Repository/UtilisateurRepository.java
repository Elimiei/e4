package API.Repository;

import API.Tables.Utilisateur;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.rmi.CORBA.Util;

@EnableJpaRepositories
@Repository
public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long> {

    public Utilisateur findUtilisateurByIdentifiant(String name);
    public Utilisateur findUtilisateurByMotdepasse(String password);
}
