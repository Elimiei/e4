package API.Repository;

import API.Tables.TypeDeContrat;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@EnableJpaRepositories
@Repository
public interface TypeDeContratRepository extends CrudRepository<TypeDeContrat, Long> {
}
