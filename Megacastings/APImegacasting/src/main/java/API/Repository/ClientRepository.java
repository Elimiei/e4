package API.Repository;

import API.Tables.Client;
import API.Tables.Offre;
import API.Tables.Partenaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@EnableJpaRepositories
@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

}