package API.Service;

import API.Repository.TypeDeContratRepository;
import API.Tables.TypeDeContrat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TypeDeContratService {
    @Autowired
    private TypeDeContratRepository typeDeContratRepository;

    public List getAllTypeDeContrats() {

        List typeDeContrat = new ArrayList<>();
        typeDeContratRepository.findAll().forEach(typeDeContrat::add);

        return typeDeContrat;
    }

    public TypeDeContrat getTypeDeContrat(Long id) {
        return typeDeContratRepository.findOne(id);

    }

    public Long addTypeDeContrat(TypeDeContrat typeDeContrat) {
        return typeDeContratRepository.save(typeDeContrat).getId();
    }

    public void updateTypeDeContrat(Long id, TypeDeContrat typeDeContrat) {
        typeDeContratRepository.save(typeDeContrat);
    }

    public void deleteTypeDeContrat (Long id) {
        typeDeContratRepository.delete(id);
    }

}

