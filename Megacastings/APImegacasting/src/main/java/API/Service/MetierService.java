package API.Service;

import API.Repository.MetierRepository;
import API.Tables.Metier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MetierService {

    @Autowired
    private MetierRepository metierRepository;

    public List getAllMetiers() {

        List metiers = new ArrayList<>();
        metierRepository.findAll().forEach(metiers::add);
        System.out.println(metiers);

        return metiers;
    }

    public Metier getMetier(Long id) {
        return metierRepository.findOne(id);

    }

    public Long addMetier(Metier metier) {
        metier.setIdDomaine(metier.getIdDomaine());
        return metierRepository.save(metier).getId();
    }

    public void updateMetier(Long id, Metier metier) {
        metier.setIdDomaine(metier.getIdDomaine());
        metierRepository.save(metier);
    }

    public void deleteMetier(Long id) {
        metierRepository.delete(id);
    }

}

