package API.Service;

import API.Repository.PartenaireRepository;
import API.Tables.Partenaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PartenaireService {

    @Autowired
    private PartenaireRepository partenaireRepository;

    public List getAllPartenaires() {

        List partenaires = new ArrayList<>();
        partenaireRepository.findAll().forEach(partenaires::add);
        return partenaires;
    }

    public Partenaire getPartenaire(Long id) {
        return partenaireRepository.findOne(id);

    }

    public Long addPartenaire(Partenaire partenaire) {
        return partenaireRepository.save(partenaire).getId();
    }

    public void updatePartenaire(Long id, Partenaire partenaire) {
        partenaireRepository.save(partenaire);
    }

    public void deletePartenaire(Long id) {
        partenaireRepository.delete(id);
    }

}
