package API.Service;

import API.Repository.ClientRepository;
import API.Tables.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public List getAllClients() {

        List clients = new ArrayList<>();
        clientRepository.findAll().forEach(clients::add);

        return clients;
    }

    public Client getClient(Long id) {
        return clientRepository.findOne(id);

    }

    public Long addClient(Client client) {
        return clientRepository.save(client).getId();

    }

    public void updateClient(Long id, Client client) {
        clientRepository.save(client);
    }

    public void deleteClient(Long id) {
        clientRepository.delete(id);
    }

}
