package API.Service;

import API.Repository.DomaineDeMetierRepository;
import API.Tables.DomaineDeMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DomaineDeMetierService {

    @Autowired
    private DomaineDeMetierRepository domaineDeMetierRepository;

    public List getAllDomaineDeMetiers() {

        List domaineDeMetiers = new ArrayList<>();
        domaineDeMetierRepository.findAll().forEach(domaineDeMetiers::add);

        return domaineDeMetiers;
    }

    public DomaineDeMetier getDomaineDeMetier(Long id) {
        return domaineDeMetierRepository.findOne(id);

    }

    public Long addDomaineDeMetier(DomaineDeMetier domaineDeMetier) {
        return domaineDeMetierRepository.save(domaineDeMetier).getId();
    }

    public void updateDomaineDeMetier(Long id, DomaineDeMetier domaineDeMetier) {
        domaineDeMetierRepository.save(domaineDeMetier);
    }

    public void deleteDomaineDeMetier(Long id) {
        domaineDeMetierRepository.delete(id);
    }

}
