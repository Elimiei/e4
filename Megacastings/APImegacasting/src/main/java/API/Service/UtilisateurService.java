package API.Service;

import API.Repository.UtilisateurRepository;
import API.Tables.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UtilisateurService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    public List getAllUtilisateurs() {

        List utilisateurs = new ArrayList<>();
        utilisateurRepository.findAll().forEach(utilisateurs::add);

        return utilisateurs;
    }

    public Utilisateur getUtilisateur(Long id) {
        return utilisateurRepository.findOne(id);

    }

    public Utilisateur getUtilisateurByName(String name) {
        return utilisateurRepository.findUtilisateurByIdentifiant(name);
    }

    public Utilisateur getUtilisateurByPassword(String password) {
        return utilisateurRepository.findUtilisateurByMotdepasse(password);
    }

    public Long addUtilisateur(Utilisateur utilisateur) {
        return utilisateurRepository.save(utilisateur).getId();

    }

    public void updateUtilisateur(Long id, Utilisateur utilisateur) {
        utilisateurRepository.save(utilisateur);
    }

    public void deleteUtilisateur(Long id) {
        utilisateurRepository.delete(id);
    }
}
