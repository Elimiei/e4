package API.Service;

import API.Repository.OffreRepository;
import API.Repository.PartenaireRepository;
import API.Tables.Offre;
import API.Tables.Partenaire;
import API.Tables.PartenaireFlux;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Part;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.Console;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.security.NoSuchAlgorithmException;

@Service
public class OffreService {

    @Autowired
    private OffreRepository offreRepository;

    @Autowired
    PartenaireRepository partenaireRepository;



    public List getAllOffres() {

        List offres = new ArrayList<>();
        offreRepository.findAll().forEach(offres::add);

        return offres;
    }

    public List<Offre> getAllOffresFlux(PartenaireFlux partenaireFlux) throws NoSuchAlgorithmException{

        List<Partenaire> partenaires = new ArrayList<>();
        List<Offre> offres = new ArrayList<>();
        partenaireRepository.findAll().forEach(partenaires::add);

        for (Partenaire partenaire : partenaires) {
            String email = partenaire.getEmail();
            String mdp = partenaire.getMot_de_passe();

            if (partenaireFlux.getEmail().equals(email)) {
                if (partenaireFlux.getMot_de_passe().equals(mdp)){
                    offreRepository.findAll().forEach(offres::add);
                }
            }
        }

        return offres;
    }

    public Offre getOffre(Long id) {
        return offreRepository.findOne(id);

    }

    public Long addOffre(Offre offre) {
        offre.setIdClient(offre.getIdClient());
        offre.setIdMetier(offre.getIdMetier());
        offre.setId_Type_de_contrat(offre.getId_Type_de_contrat());
        return offreRepository.save(offre).getId();
    }

    public void updateOffre(Long id, Offre offre) {
        offreRepository.save(offre);
    }

    public void deleteOffre(Long id) {
        offreRepository.delete(id);
    }

}
