package API.Tables;

import javax.persistence.*;

@Entity(name = "Client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    private String nom_organisation;
    private int telephone;
    private String email;
    private Long numero_siret;
    private String ligne_adresse;
    private int code_postal;
    private String nom_ville;
    private String nom_pays;

    public Client() {
    }

    public Client(String nom_organisation, int telephone, String email, Long numero_siret, String ligne_adresse, int code_postal, String nom_ville, String nom_pays) {
        this.nom_organisation = nom_organisation;
        this.telephone = telephone;
        this.email = email;
        this.numero_siret = numero_siret;
        this.ligne_adresse = ligne_adresse;
        this.code_postal = code_postal;
        this.nom_ville = nom_ville;
        this.nom_pays = nom_pays;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom_organisation() {
        return nom_organisation;
    }

    public void setNom_organisation(String nom_organisation) {
        this.nom_organisation = nom_organisation;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getNumero_siret() {
        return numero_siret;
    }

    public void setNumero_siret(Long numero_siret) {
        this.numero_siret = numero_siret;
    }

    public String getLigne_adresse() {
        return ligne_adresse;
    }

    public void setLigne_adresse(String ligne_adresse) {
        this.ligne_adresse = ligne_adresse;
    }

    public int getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(int code_postal) {
        this.code_postal = code_postal;
    }

    public String getNom_ville() {
        return nom_ville;
    }

    public void setNom_ville(String nom_ville) {
        this.nom_ville = nom_ville;
    }

    public String getNom_pays() {
        return nom_pays;
    }

    public void setNom_pays(String nom_pays) {
        this.nom_pays = nom_pays;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", nom_organisation='" + nom_organisation + '\'' +
                ", telephone=" + telephone +
                ", email='" + email + '\'' +
                ", numero_siret=" + numero_siret +
                ", ligne_adresse='" + ligne_adresse + '\'' +
                ", code_postal=" + code_postal +
                ", nom_ville='" + nom_ville + '\'' +
                ", nom_pays='" + nom_pays + '\'' +
                '}';
    }
}
