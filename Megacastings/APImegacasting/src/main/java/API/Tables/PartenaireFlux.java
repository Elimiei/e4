package API.Tables;

import java.util.Date;

public class PartenaireFlux {

    private String email;
    private String mot_de_passe;

    public PartenaireFlux(String email, String mot_de_passe) {
        this.email = email;
        this.mot_de_passe = mot_de_passe;
    }

    public PartenaireFlux() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMot_de_passe() {
        return mot_de_passe;
    }

    public void setMot_de_passe(String mot_de_passe) {
        this.mot_de_passe = mot_de_passe;
    }

    @Override
    public String toString() {
        return "Partenaire{" +
                "email='" + email + '\'' +
                ", mot_de_passe='" + mot_de_passe + '\'' +
                '}';
    }
}
