package API.Tables;

import javax.persistence.*;

@Entity(name = "Type_de_contrat")
public class TypeDeContrat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;
    private String libelle;
    private String code;


    public TypeDeContrat() {
    }

    public TypeDeContrat(String libelle, String code) {
        this.libelle = libelle;
        this.code = code;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
