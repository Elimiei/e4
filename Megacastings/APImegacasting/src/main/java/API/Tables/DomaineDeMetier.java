package API.Tables;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity(name = "Domaine_de_metier")
public class DomaineDeMetier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    Long id;
    private String libelle;



    public DomaineDeMetier(String libelle) {
        this.libelle = libelle;
    }

    public DomaineDeMetier() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return "DomaineDeMetier{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                '}';
    }
}
