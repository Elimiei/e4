package API.Tables;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
public class Metier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    Long Id;
    private String libelle;

    // prendre ça comme exemple pour le reste après
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_Domaine_de_metier", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private DomaineDeMetier domaineDeMetier;

    @JsonIgnore
    @Column(name = "id_Domaine_de_metier")
    private Long idDomaine;

    public Metier(String libelle) {
        this.libelle = libelle;
    }

    public Metier() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }


    public DomaineDeMetier getDomaineDeMetier() {
        return domaineDeMetier;
    }

    public void setDomaineDeMetier(DomaineDeMetier domaineDeMetier) {
        this.domaineDeMetier = domaineDeMetier;
    }

    public Long getIdDomaine() {
        return domaineDeMetier.getId();
    }

    public void setIdDomaine(Long idDomaine) {
        this.idDomaine = idDomaine;
    }
}
