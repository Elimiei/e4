package API.Tables;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Partenaire")
public class Partenaire {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;
    private String nom_organisation;
    private Date  date_partenariat;
    private String email;
    private String mot_de_passe;

    public Partenaire(String nom_organisation, Date date_partenariat, String email, String mot_de_passe) {
        this.nom_organisation = nom_organisation;
        this.date_partenariat = date_partenariat;
        this.email = email;
        this.mot_de_passe = mot_de_passe;
    }

    public Partenaire() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom_organisation() {
        return nom_organisation;
    }

    public void setNom_organisation(String nom_organisation) {
        this.nom_organisation = nom_organisation;
    }

    public Date getDate_partenariat() {
        return date_partenariat;
    }

    public void setDate_partenariat(Date date_partenariat) {
        this.date_partenariat = date_partenariat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMot_de_passe() {
        return mot_de_passe;
    }

    public void setMot_de_passe(String mot_de_passe) {
        this.mot_de_passe = mot_de_passe;
    }

    @Override
    public String toString() {
        return "Partenaire{" +
                "id=" + id +
                ", nom_organisation='" + nom_organisation + '\'' +
                ", date_partenariat=" + date_partenariat +
                ", email='" + email + '\'' +
                ", mot_de_passe='" + mot_de_passe + '\'' +
                '}';
    }
}
