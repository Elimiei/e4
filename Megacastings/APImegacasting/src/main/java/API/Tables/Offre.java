package API.Tables;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;


@Entity(name = "Offre")
public class Offre{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;
    private String libelle;
    private Date date_publication;
    private Integer duree_publication;
    private Date date_debut_contrat;
    private Integer nombre_poste;
    private String description_poste;
    private String description_profil;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_Client", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Client client;

    @JsonIgnore
    @Column(name = "id_Client")
    private Long idClient;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_Metier", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Metier metier;

    @JsonIgnore
    @Column(name = "id_Metier")
    private Long idMetier;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_Type_de_contrat", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private TypeDeContrat typeDeContrat;

    @JsonIgnore
    @Column(name = "id_Type_de_contrat")
    private Long id_Type_de_contrat;

    public Offre() {}

    public Offre(String libelle, Date date_publication, Integer duree_publication, Date date_debut_contrat, Integer nombre_poste, String description_poste, String description_profil, Long id_Client, Long id_Metier, Long id_Type_de_contrat) {
        this.libelle = libelle;
        this.date_publication = date_publication;
        this.duree_publication = duree_publication;
        this.date_debut_contrat = date_debut_contrat;
        this.nombre_poste = nombre_poste;
        this.description_poste = description_poste;
        this.description_profil = description_profil;

    }


    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }


    public Date getDate_publication() {
        return date_publication;
    }

    public void setDate_publication(Date date_publication) {
        this.date_publication = date_publication;
    }

    public Integer getDuree_publication() {
        return duree_publication;
    }

    public void setDuree_publication(Integer duree_publication) {
        this.duree_publication = duree_publication;
    }

    public Date getDate_debut_contrat() {
        return date_debut_contrat;
    }

    public void setDate_debut_contrat(Date date_debut_contrat) {
        this.date_debut_contrat = date_debut_contrat;
    }

    public Integer getNombre_poste() {
        return nombre_poste;
    }

    public void setNombre_poste(Integer nombre_poste) {
        this.nombre_poste = nombre_poste;
    }

    public String getDescription_poste() {
        return description_poste;
    }

    public void setDescription_poste(String description_poste) {
        this.description_poste = description_poste;
    }

    public String getDescription_profil() {
        return description_profil;
    }

    public void setDescription_profil(String description_profil) {
        this.description_profil = description_profil;
    }

    @Override
    public String toString() {
        return "Offre{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", date_publication=" + date_publication +
                ", dureee_publication=" + duree_publication +
                ", date_debut_contrat=" + date_debut_contrat +
                ", nombre_poste=" + nombre_poste +
                ", description_poste='" + description_poste + '\'' +
                ", description_profil='" + description_profil + '\'' +
                '}';
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Metier getMetier() {
        return metier;
    }

    public void setMetier(Metier metier) {
        this.metier = metier;
    }

    public TypeDeContrat getTypeDeContrat() {
        return typeDeContrat;
    }

    public void setTypeDeContrat(TypeDeContrat typeDeContrat) {
        this.typeDeContrat = typeDeContrat;
    }

    public Long getIdClient() {
        return client.getId();
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public Long getIdMetier() {
        return metier.getId();
    }

    public void setIdMetier(Long idMetier) {
        this.idMetier = idMetier;
    }

    public Long getId_Type_de_contrat() {
        return typeDeContrat.getId();
    }

    public void setId_Type_de_contrat(Long id_Type_de_contrat) {
        this.id_Type_de_contrat = id_Type_de_contrat;
    }
}
