package API.Controllers;

import API.Service.UtilisateurService;
import API.Tables.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UtilisateurController {

    @Autowired
    private UtilisateurService utilisateurService;

    @RequestMapping(value = "/utilisateurs", method = RequestMethod.GET)
    public List getAllUtilisateurs(){
        return utilisateurService.getAllUtilisateurs();
    }

    @RequestMapping(value = "/utilisateurs/{id}", method = RequestMethod.GET)
    public Utilisateur getUtilisateur(@PathVariable Long id) {
        return utilisateurService.getUtilisateur(id);
    }

    @RequestMapping(value = "/utilisateurs/find/{name}", method = RequestMethod.GET)
    public Utilisateur getUtilisateurByName(@PathVariable String name) {
        return utilisateurService.getUtilisateurByName(name);
    }

    @RequestMapping(value = "/utilisateurs/password/{password}", method = RequestMethod.GET)
    public Utilisateur getUtilisateurByPassword(@PathVariable String password) {
        return utilisateurService.getUtilisateurByPassword(password);
    }


    @PostMapping(value = "/utilisateurs")
    public Long addUtilisateur(@RequestBody Utilisateur utilisateur) {
        return utilisateurService.addUtilisateur(utilisateur);
    }

    @RequestMapping(value = "/utilisateurs/{id}", method = RequestMethod.PUT)
    public void updateUtilisateur(@RequestBody Utilisateur utilisateur,@PathVariable Long id) {
        utilisateurService.updateUtilisateur(id, utilisateur);
    }


    @DeleteMapping("utilisateurs/{id}")
    public boolean deleteUtilisateur(@PathVariable Long id){
        utilisateurService.deleteUtilisateur(id);
        return true;
    }

}
