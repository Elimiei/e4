package API.Controllers;

import API.Service.OffreService;
import API.Tables.Offre;
import API.Tables.PartenaireFlux;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class OffreController {

    @Autowired
    private OffreService offreService;

    private PartenaireFlux partenaireFlux;

    @RequestMapping(value = "/offres", method = RequestMethod.GET)
    public List getAllOffres(){
        return offreService.getAllOffres();
    }

    @RequestMapping(value = "/offres/{id}", method = RequestMethod.GET)
    public Offre getOffre(@PathVariable Long id) {
        return offreService.getOffre(id);
    }

    @PostMapping(value = "/offres")
    public Long addOffre(@RequestBody Offre offre) {
        return offreService.addOffre(offre);
    }

    @RequestMapping(value = "/offres/{id}", method = RequestMethod.PUT)
    public void updateOffre(@RequestBody Offre offre,@PathVariable Long id) {
        offreService.updateOffre(id, offre);
    }

    @DeleteMapping("offres/{id}")
    public boolean deleteOffre(@PathVariable Long id){
        offreService.deleteOffre(id);
        return true;
    }

    @PostMapping(value = "/offres/flux")
    public List<Offre> addPartenaire(@RequestBody PartenaireFlux partenaire) {
        this.partenaireFlux = partenaire;

        List<Offre> offres = new ArrayList<>();
        if (this.partenaireFlux != null){
            try {
                offres = offreService.getAllOffresFlux(partenaireFlux);
            } catch (NoSuchAlgorithmException e) {
                System.err.println("Erreur lors du cryptage");
            }
        }
        return offres;
    }

}
