package API.Controllers;

import API.Repository.ClientRepository;
import API.Service.ClientService;
import API.Tables.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ClientController {

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public List getAllClients(){
        return clientService.getAllClients();
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.GET)
    public Client getClient(@PathVariable Long id) {
        return clientService.getClient(id);
    }

    @PostMapping(value = "/clients")
    public Long addClient(@RequestBody Client client) {
        return clientService.addClient(client);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    public void updateClient(@RequestBody Client client,@PathVariable Long id) {
        clientService.updateClient(id, client);
    }

    @DeleteMapping("clients/{id}")
    public boolean deleteClient(@PathVariable Long id){
        clientService.deleteClient(id);
        return true;
    }
}