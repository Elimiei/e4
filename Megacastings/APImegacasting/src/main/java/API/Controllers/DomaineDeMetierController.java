package API.Controllers;

import API.Service.DomaineDeMetierService;
import API.Tables.DomaineDeMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class DomaineDeMetierController {

    @Autowired
    private DomaineDeMetierService domaineDeMetierService;

    @RequestMapping(value = "/domainesdemetier", method = RequestMethod.GET)
    public List getAllDomaineDeMetiers(){
        return domaineDeMetierService.getAllDomaineDeMetiers();
    }

    @RequestMapping(value = "/domainesdemetier/{id}", method = RequestMethod.GET)
    public DomaineDeMetier getDomaineDeMetier(@PathVariable Long id) {
        return domaineDeMetierService.getDomaineDeMetier(id);
    }

    @PostMapping(value = "/domainesdemetier")
    public Long addDomaineDeMetier(@RequestBody DomaineDeMetier domaineDeMetier) {
        return domaineDeMetierService.addDomaineDeMetier(domaineDeMetier);
    }

    @RequestMapping(value = "/domainesdemetier/{id}", method = RequestMethod.PUT)
    public void updateDomaineDeMetier(@RequestBody DomaineDeMetier domaineDeMetier,@PathVariable Long id) {
        domaineDeMetierService.updateDomaineDeMetier(id, domaineDeMetier);
    }

    @DeleteMapping("domainesdemetier/{id}")
    public boolean deleteDomaineDeMetier(@PathVariable Long id){
        domaineDeMetierService.deleteDomaineDeMetier(id);
        return true;
    }
}
