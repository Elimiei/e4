package API.Controllers;

import API.Service.PartenaireService;
import API.Tables.Partenaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class PartenaireController {

    @Autowired
    private PartenaireService partenaireService;

    @RequestMapping(value = "/partenaires", method = RequestMethod.GET)
    public List getAllPartenaires(){
        return partenaireService.getAllPartenaires();
    }

    @RequestMapping(value = "/partenaires/{id}", method = RequestMethod.GET)
    public Partenaire getPartenaire(@PathVariable Long id) {
        return partenaireService.getPartenaire(id);
    }

    @PostMapping(value = "/partenaires")
    public Long addPartenaire(@RequestBody Partenaire partenaire) {
        return partenaireService.addPartenaire(partenaire);
    }

    @RequestMapping(value = "/partenaires/{id}", method = RequestMethod.PUT)
    public void updatePartenaire(@RequestBody Partenaire partenaire,@PathVariable Long id) {
        partenaireService.updatePartenaire(id, partenaire);
    }

    @DeleteMapping("partenaires/{id}")
    public boolean deletePartenaire(@PathVariable Long id){
        partenaireService.deletePartenaire(id);
        return true;
    }


}
