package API.Controllers;

import API.Service.TypeDeContratService;
import API.Tables.TypeDeContrat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class TypeDeContratController {

    @Autowired
    private TypeDeContratService typeDeContratService;

    @RequestMapping(value = "/typesdecontrat", method = RequestMethod.GET)
    public List getAllTypeDeContrat(){
        return typeDeContratService.getAllTypeDeContrats();
    }

    @RequestMapping(value = "/typesdecontrat/{id}", method = RequestMethod.GET)
    public TypeDeContrat getTypeDeContrat(@PathVariable Long id) {
        return typeDeContratService.getTypeDeContrat(id);
    }

    @PostMapping(value = "/typesdecontrat")
    public Long addTypeDeContrat(@RequestBody TypeDeContrat typeDeContrat) {
       return typeDeContratService.addTypeDeContrat(typeDeContrat);
    }

    @RequestMapping(value = "/typesdecontrat/{id}", method = RequestMethod.PUT)
    public void updateTypeDeContrat(@RequestBody TypeDeContrat typeDeContrat,@PathVariable Long id) {
        typeDeContratService.updateTypeDeContrat(id, typeDeContrat);
    }

    @DeleteMapping("typesdecontrat/{id}")
    public boolean deleteTypeDeContrat(@PathVariable Long id){
        typeDeContratService.deleteTypeDeContrat(id);
        return true;
    }
}
