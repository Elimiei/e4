package API.Controllers;

import API.Service.MetierService;
import API.Tables.Metier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class MetierController {

    public List<Metier> metiers;
    @Autowired
    private MetierService metierService;

    @RequestMapping(value = "/metiers", method = RequestMethod.GET)
    public List getAllMetiers(){
        metiers =  metierService.getAllMetiers();
        return metierService.getAllMetiers();
    }

    @RequestMapping(value = "/metiers/{id}", method = RequestMethod.GET)
    public Metier getMetier(@PathVariable Long id) {
        return metierService.getMetier(id);
    }

    @PostMapping(value = "/metiers")
    public Long addMetier(@RequestBody Metier metier) {
       return metierService.addMetier(metier);
    }

    @RequestMapping(value = "/metiers/{id}", method = RequestMethod.PUT)
    public void updateMetier(@RequestBody Metier metier,@PathVariable Long id) {
        metierService.updateMetier(id, metier);
    }

    @DeleteMapping("metiers/{id}")
    public boolean deleteMetier(@PathVariable Long id){
        metierService.deleteMetier(id);
        return true;
    }
}
