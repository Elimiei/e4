package Controllers;

import Implementation.ImplDomaineDeMetier;
import Implementation.ImplTypeDeContrat;
import Interfaces.CRUDInterface;
import Models.DomaineDeMetierModel;
import Models.TypeDeContratModel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.ResourceBundle;

public class TypeDeContratController implements Initializable {

    private String codeStatut;

    ObservableList<TypeDeContratModel> listDDM;

    CRUDInterface<TypeDeContratModel> crudData = new ImplTypeDeContrat();

    @FXML
    private TableView<TypeDeContratModel> tdcView;

    @FXML
    private TableColumn<TypeDeContratModel, String> idColumn;

    @FXML
    private TableColumn<TypeDeContratModel, String> libelleColumn;

    @FXML
    private Pane tdcPane;

    @FXML
    private Label topLeftLabel;

    @FXML
    private Label tdcID;

    @FXML
    private JFXTextField libelle;

    @FXML
    private JFXTextField code;

    @FXML
    private JFXButton reset;

    @FXML
    private JFXButton save;

    @FXML
    private JFXButton deleteButton;

    @FXML
    void clicTableData(MouseEvent event) {
        codeStatut = "1";
        try {
            TypeDeContratModel clic = tdcView.getSelectionModel().getSelectedItems().get(0);
            tdcID.setText(clic.getId());
            libelle.setText(clic.getLibelle());
            code.setText(clic.getCode());
        } catch (Exception e) {
        }
    }

    @FXML
    void delete(ActionEvent event) {
        TypeDeContratModel clic = tdcView.getSelectionModel().getSelectedItems().get(0);
        crudData.delete(clic.getId());
        displayData();
        clear();

    }

    @FXML
    void reset(ActionEvent event) {

    }

    @FXML
    void save(ActionEvent event) {

        TypeDeContratModel newDDM = new TypeDeContratModel();
        newDDM.setLibelle(libelle.getText());
        newDDM.setCode(code.getText());
        if (tdcID.getText() != null) {
            newDDM.setId(tdcID.getText());
        }
        if (codeStatut.equals("0")) {
            crudData.insert(newDDM);
            dialog(Alert.AlertType.INFORMATION, "Donnée ajoutée");
        } else {
            crudData.update(newDDM);
            dialog(Alert.AlertType.INFORMATION, "Donnée modifiée");
        }
        displayData();
        clear();
        setId();

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        idColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<TypeDeContratModel, String> cellData) ->
                        cellData.getValue().idProperty());
        libelleColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<TypeDeContratModel, String> cellData) ->
                        cellData.getValue().libelleProperty());
        codeStatut = "0";
        displayData();
        setId();
        tdcView.getSelectionModel().clearSelection();

    }

    private void dialog(Alert.AlertType alertType, String s) {
        Alert alert = new Alert(alertType, s);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Info");
        alert.showAndWait();
    }

    private void clear() {
        tdcID.setText(null);
        libelle.clear();
        code.clear();
        this.codeStatut = "0";
    }

    private void setId() {
        TypeDeContratModel typeDeContratModel = new TypeDeContratModel();
        tdcID.setText(typeDeContratModel.getId());
    }

    private void displayData() {
        listDDM = crudData.getAll();
        System.out.println(listDDM);
        tdcView.setItems(listDDM);
    }
}
