package Controllers;

import Implementation.ImplPartenaire;
import Interfaces.CRUDInterface;
import Models.PartenaireModel;
import com.jfoenix.controls.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class PartenaireController implements Initializable {

    private String codeStatut;
    ObservableList<PartenaireModel> listPartenaires;

    CRUDInterface<PartenaireModel> crudData = new ImplPartenaire();

    @FXML
    private TableView<PartenaireModel> PartenairesView;

    @FXML
    private TableColumn<PartenaireModel, String> idColumn;

    @FXML
    private TableColumn<PartenaireModel, String> nomOrganisationColumn;

    @FXML
    private TableColumn<PartenaireModel, String> datePartenariatColumn;

    @FXML
    private TableColumn<PartenaireModel, String> emailColumn;

    @FXML
    private Pane partenairePane;

    @FXML
    private Label topLeftLabel;

    @FXML
    private Label partenaireID;

    @FXML
    private JFXTextField nom_organisation;

    @FXML
    private JFXDatePicker date_partenariat;

    @FXML
    private JFXTextField email;

    @FXML
    private JFXPasswordField password;

    @FXML
    private JFXButton reset;

    @FXML
    private JFXButton save;

    @FXML
    private JFXButton deleteButton;


    @FXML
    private void clicTableData(MouseEvent event) {
        codeStatut = "1";
        try {
            PartenaireModel clic = PartenairesView.getSelectionModel().getSelectedItems().get(0);
            partenaireID.setText(clic.getId());
            nom_organisation.setText(clic.getNom_organisation());
            email.setText(clic.getEmail());
            password.setText(clic.getMot_de_passe());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            date_partenariat.setValue(LocalDate.parse(clic.getFormatDate(), formatter));
        } catch (Exception e) {
        }

    }

    @FXML
    void save(ActionEvent event) {
        PartenaireModel newPartenaire = new PartenaireModel();
        newPartenaire.setNom_organisation(nom_organisation.getText());
        newPartenaire.setEmail(email.getText());
        newPartenaire.setMot_de_passe(password.getText());
        Date date = java.sql.Date.valueOf(date_partenariat.getValue());
        if (partenaireID.getText() != null) {
            newPartenaire.setId(partenaireID.getText());
        }
        newPartenaire.setDate_partenariat(date);
        if (codeStatut.equals("0")) {
            crudData.insert(newPartenaire);
            dialog(Alert.AlertType.INFORMATION, "Donnée ajoutée");
        } else {
            crudData.update(newPartenaire);
            dialog(Alert.AlertType.INFORMATION, "Donnée modifiée");
        }
        displayData();
        clear();
        setId();
    }

    @FXML
    void delete(ActionEvent event) {
        PartenaireModel clic = PartenairesView.getSelectionModel().getSelectedItems().get(0);
        crudData.delete(clic.getId());
        displayData();
        clear();
    }

    @FXML
    void reset(ActionEvent event) {
        clear();
    }

    private void dialog(Alert.AlertType alertType, String s) {
        Alert alert = new Alert(alertType, s);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Info");
        alert.showAndWait();
    }

    private void clear() {
        partenaireID.setText(null);
        nom_organisation.clear();
        email.clear();
        password.clear();
        date_partenariat.setValue(null);
        this.codeStatut = "0";
    }

    private void setId() {
        PartenaireModel partenaireModel = new PartenaireModel();
        partenaireID.setText(partenaireModel.getId());
    }

    private void displayData() {
        listPartenaires = crudData.getAll();
        System.out.println(listPartenaires);
        PartenairesView.setItems(listPartenaires);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<PartenaireModel, String> cellData) ->
                        cellData.getValue().idProperty());
        nomOrganisationColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<PartenaireModel, String> cellData) ->
                        cellData.getValue().nom_organisationProperty());
        datePartenariatColumn.setCellValueFactory(
                (new PropertyValueFactory("formatDate")));
        emailColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<PartenaireModel, String> cellData) ->
                        cellData.getValue().emailProperty());
        listPartenaires = FXCollections.observableArrayList();
        date_partenariat.setValue(null);
        codeStatut = "0";
        displayData();
        setId();
        PartenairesView.getSelectionModel().clearSelection();
    }
}
