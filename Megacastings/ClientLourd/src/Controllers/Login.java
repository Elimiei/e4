package Controllers;

import Implementation.ImplUtilisateur;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import java.io.IOException;

public class Login {
    @FXML
    private JFXTextField identifiant;

    @FXML
    private com.jfoenix.controls.JFXPasswordField mot_de_passe;

    @FXML
    private JFXButton connexionButton;

    @FXML
    private Pane loginPane;

    @FXML
    void connect(ActionEvent event) {

        ImplUtilisateur implUtilisateur = new ImplUtilisateur();
        boolean res;

        if (mot_de_passe != null && identifiant != null) {
            res = implUtilisateur.isExistingUser(identifiant.getText(), mot_de_passe.getText());

            if (res){
                try {
                    Pane offrePane = FXMLLoader.load(getClass().getResource("/Resources/gestionOnglets.fxml"));
                    loginPane.getChildren().add(offrePane);
                } catch (IOException e) {

                }
            } else {

                System.out.println("Utilisateur inexistant");

            }

        }


    }

}
