package Controllers;

import Implementation.ImplDomaineDeMetier;
import Interfaces.CRUDInterface;
import Models.DomaineDeMetierModel;
import Models.PartenaireModel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class DomaineDeMetierController implements Initializable {

    private String codeStatut;

    ObservableList<DomaineDeMetierModel> listDDM;

    CRUDInterface<DomaineDeMetierModel> crudData = new ImplDomaineDeMetier();

    @FXML
    private TableView<DomaineDeMetierModel> ddmView;

    @FXML
    private TableColumn<DomaineDeMetierModel, String> idColumn;

    @FXML
    private TableColumn<DomaineDeMetierModel, String> libelleColumn;

    @FXML
    private Pane partenairePane;

    @FXML
    private Label topLeftLabel;

    @FXML
    private Label ddmID;

    @FXML
    private JFXTextField libelle;

    @FXML
    private JFXButton reset;

    @FXML
    private JFXButton save;

    @FXML
    private JFXButton deleteButton;

    @FXML
    void clicTableData(MouseEvent event) {
        codeStatut = "1";
        try {
            DomaineDeMetierModel clic = ddmView.getSelectionModel().getSelectedItems().get(0);
            ddmID.setText(clic.getId());
            libelle.setText(clic.getLibelle());
        } catch (Exception e) {
        }
    }

    @FXML
    void delete(ActionEvent event) {
        DomaineDeMetierModel clic = ddmView.getSelectionModel().getSelectedItems().get(0);
        crudData.delete(clic.getId());
        displayData();
        clear();
    }

    @FXML
    void reset(ActionEvent event) {
        clear();
    }

    @FXML
    void save(ActionEvent event) {

        DomaineDeMetierModel newDDM = new DomaineDeMetierModel();
        newDDM.setLibelle(libelle.getText());
        if (ddmID.getText() != null) {
            newDDM.setId(ddmID.getText());
        }
        if (codeStatut.equals("0")) {
            crudData.insert(newDDM);
            dialog(Alert.AlertType.INFORMATION, "Donnée ajoutée");
        } else {
            crudData.update(newDDM);
            dialog(Alert.AlertType.INFORMATION, "Donnée modifiée");
        }
        displayData();
        clear();
        setId();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        idColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<DomaineDeMetierModel, String> cellData) ->
                        cellData.getValue().idProperty());
        libelleColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<DomaineDeMetierModel, String> cellData) ->
                        cellData.getValue().libelleProperty());
        codeStatut = "0";
        displayData();
        setId();
        ddmView.getSelectionModel().clearSelection();

    }

    private void dialog(Alert.AlertType alertType, String s) {
        Alert alert = new Alert(alertType, s);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Info");
        alert.showAndWait();
    }

    private void clear() {
        ddmID.setText(null);
        libelle.clear();
        this.codeStatut = "0";
    }

    private void setId() {
        DomaineDeMetierModel domaineDeMetierModel = new DomaineDeMetierModel();
        ddmID.setText(domaineDeMetierModel.getId());
    }

    private void displayData() {
        listDDM = crudData.getAll();
        System.out.println(listDDM);
        ddmView.setItems(listDDM);
    }
}
