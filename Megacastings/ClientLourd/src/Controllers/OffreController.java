package Controllers;

import Implementation.*;
import Interfaces.CRUDInterface;
import Models.*;
import com.jfoenix.controls.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;

public class OffreController implements Initializable {

    private String codeStatut;

    ImplOffre implOffre = new ImplOffre();
    ImplClient implClient = new ImplClient();
    ImplMetier implMetier = new ImplMetier();
    ImplTypeDeContrat implTypeDeContrat = new ImplTypeDeContrat();

    ObservableList<OffreModel> listOffres;

    CRUDInterface<OffreModel> crudData = new ImplOffre();

    ObservableList<ClientModel> clientNames = FXCollections.observableList(implClient.getAll());
    ObservableList<MetierModel> metierNames = FXCollections.observableList(implMetier.getAll());
    ObservableList<TypeDeContratModel> typeDeContratNames = FXCollections.observableList(implTypeDeContrat.getAll());

    @FXML
    private TableView<OffreModel> OffresView;

    @FXML
    private TableColumn<OffreModel, String> idColumn;

    @FXML
    private TableColumn<OffreModel, String> libelleColumn;

    @FXML
    private TableColumn<OffreModel, String> datePublicationColumn;

    @FXML
    private TableColumn<OffreModel, String> dureePublicationColumn;

    @FXML
    private TableColumn<OffreModel, String> dateDEbutContratColumn;

    @FXML
    private TableColumn<OffreModel, String> nombrePostesColumn;

    @FXML
    private Pane offrePane;

    @FXML
    private Label topLeftLabel;

    @FXML
    private Label offreID;

    @FXML
    private JFXTextField libelle;

    @FXML
    private JFXDatePicker date_publication;

    @FXML
    private JFXTextField duree_publication;

    @FXML
    private JFXDatePicker date_debut_contrat;

    @FXML
    private JFXTextField nombre_postes;

    @FXML
    private JFXTextField description_poste;

    @FXML
    private JFXTextField description_profil;

    @FXML
    private JFXComboBox<ClientModel> client;

    @FXML
    private JFXComboBox<TypeDeContratModel> type_contrat;

    @FXML
    private JFXComboBox<MetierModel> metier;

    @FXML
    private JFXButton reset;

    @FXML
    private JFXButton save;

    @FXML
    private JFXButton deleteButton;

    @FXML
    void clicTableData(MouseEvent event) {
        codeStatut = "1";
        try {
            OffreModel clic = OffresView.getSelectionModel().getSelectedItems().get(0);
            offreID.setText(clic.getId());
            libelle.setText(clic.getLibelle());
            description_poste.setText(clic.getDescription_poste());
            description_profil.setText(clic.getDescription_profil());
            nombre_postes.setText(clic.getNombre_poste());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            date_publication.setValue(LocalDate.parse(clic.getFormatDatePublication(), formatter));
            date_debut_contrat.setValue(LocalDate.parse(clic.getFormatDateDebContrat(), formatter));
            client.setItems(clientNames);
            client.setValue(clic.getClient());
        } catch (Exception e) {
        }
    }

    @FXML
    void delete(ActionEvent event) {
        OffreModel clic = OffresView.getSelectionModel().getSelectedItems().get(0);
        crudData.delete(clic.getId());
        displayData();
        clear();

    }

    @FXML
    void reset(ActionEvent event) {
        clear();
    }

    @FXML
    void save(ActionEvent event) {
        OffreModel newOffre = new OffreModel();
        newOffre.setLibelle(libelle.getText());
        newOffre.setDuree_publication(duree_publication.getText());
        newOffre.setNombre_poste(nombre_postes.getText());
        Date dateDebContrat = java.sql.Date.valueOf(date_debut_contrat.getValue());
        newOffre.setDate_debut_contrat(dateDebContrat);
        Date datePubli = java.sql.Date.valueOf(date_publication.getValue());
        newOffre.setDate_publication(datePubli);
        newOffre.setDescription_poste(description_poste.getText());
        newOffre.setDescription_profil(description_profil.getText());
        newOffre.setMetier(implMetier.getById(metier.getValue().getId()));
        newOffre.setTypeDeContrat(implTypeDeContrat.getById(type_contrat.getValue().getId()));
        newOffre.setClient(implClient.getById(client.getValue().getId()));
        if (offreID.getText() != null) {
            newOffre.setId(offreID.getText());
        }
        if (codeStatut.equals("0")) {
            crudData.insert(newOffre);
            dialog(Alert.AlertType.INFORMATION, "Donnée ajoutée");
        } else {
            crudData.update(newOffre);
            dialog(Alert.AlertType.INFORMATION, "Donnée modifiée");
        }
        displayData();
        clear();
        setId();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        client.setItems(clientNames);
        client.setVisibleRowCount(5);

        type_contrat.setItems(typeDeContratNames);
        type_contrat.setVisibleRowCount(5);

        metier.setItems(metierNames);
        metier.setVisibleRowCount(5);

        idColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<OffreModel, String> cellData) ->
                        cellData.getValue().idProperty());
        libelleColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<OffreModel, String> cellData) ->
                        cellData.getValue().libelleProperty());
        datePublicationColumn.setCellValueFactory(
                (new PropertyValueFactory("formatDatePublication")));
        nombrePostesColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<OffreModel, String> cellData) ->
                        cellData.getValue().nombre_posteProperty());
        dureePublicationColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<OffreModel, String> cellData) ->
                        cellData.getValue().duree_publicationProperty());
        dateDEbutContratColumn.setCellValueFactory(
                (new PropertyValueFactory("formatDateDebContrat")));
        codeStatut = "0";
        displayData();
        setId();
        OffresView.getSelectionModel().clearSelection();
    }

    private void dialog(Alert.AlertType alertType, String s) {
        Alert alert = new Alert(alertType, s);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Info");
        alert.showAndWait();
    }

    private void clear() {
        offreID.setText(null);
        client.setValue(null);
        libelle.clear();
        this.codeStatut = "0";
    }

    private void setId() {
        DomaineDeMetierModel domaineDeMetierModel = new DomaineDeMetierModel();
        offreID.setText(domaineDeMetierModel.getId());
    }

    private void displayData() {
        listOffres = crudData.getAll();
        OffresView.setItems(listOffres);
    }
}
