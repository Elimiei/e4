package Controllers;

import Implementation.ImplDomaineDeMetier;
import Implementation.ImplMetier;
import Interfaces.CRUDInterface;
import Models.DomaineDeMetierModel;
import Models.MetierModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import java.io.Serializable;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MetierController implements Initializable, Serializable {


    private String codeStatut;

    ImplDomaineDeMetier implDomaineDeMetier = new ImplDomaineDeMetier();

    ObservableList<MetierModel> listDDM;

    CRUDInterface<MetierModel> crudData = new ImplMetier();

    DomaineDeMetierModel domaineDeMetierModel;

    ObservableList<DomaineDeMetierModel> ddmsNames = FXCollections.observableList(implDomaineDeMetier.getAll());

    @FXML
    private Pane metierPane;

    @FXML
    private TableView<MetierModel> MetierView;

    @FXML
    private TableColumn<MetierModel, String> idColumn;

    @FXML
    private TableColumn<MetierModel, String> libelleColumn;

    @FXML
    private TableColumn<MetierModel, String> ddmColumn;

    @FXML
    private Label topLeftLabel;

    @FXML
    private Label metierID;

    @FXML
    private JFXTextField libelle;

    @FXML
    private JFXComboBox<DomaineDeMetierModel> ddm;

    @FXML
    private JFXButton reset;

    @FXML
    private JFXButton save;

    @FXML
    private JFXButton deleteButton;

    @FXML
    void clicTableData(MouseEvent event) {
        codeStatut = "1";
        try {
            MetierModel clic = MetierView.getSelectionModel().getSelectedItems().get(0);
            domaineDeMetierModel = clic.getDomaine_de_metier();
            metierID.setText(clic.getId());
            libelle.setText(clic.getLibelle());
            ddm.setItems(ddmsNames);
            ddm.setValue(domaineDeMetierModel);
        } catch (Exception e) {
        }
    }

    @FXML
    void delete(ActionEvent event) {
        MetierModel clic = MetierView.getSelectionModel().getSelectedItems().get(0);
        crudData.delete(clic.getId());
        displayData();
        clear();
    }

    @FXML
    void reset(ActionEvent event) {
        clear();
    }

    @FXML
    void save(ActionEvent event) {

        MetierModel newMetier = new MetierModel();
        newMetier.setLibelle(libelle.getText());
        newMetier.setDomaine_de_metier(implDomaineDeMetier.getById(ddm.getValue().getId()));
        if (metierID.getText() != null) {
            newMetier.setId(metierID.getText());
        }
        if (codeStatut.equals("0")) {
            crudData.insert(newMetier);
            dialog(Alert.AlertType.INFORMATION, "Donnée ajoutée");
        } else {
            crudData.update(newMetier);
            dialog(Alert.AlertType.INFORMATION, "Donnée modifiée");
        }
        displayData();
        clear();
        setId();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ddm.setItems(ddmsNames);
        ddm.setVisibleRowCount(5);

        idColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<MetierModel, String> cellData) ->
                        cellData.getValue().idProperty());
        libelleColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<MetierModel, String> cellData) ->
                        cellData.getValue().libelleProperty());
        ddmColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<MetierModel, String> cellData) ->
                        cellData.getValue().getDomaine_de_metier().libelleProperty());
        codeStatut = "0";
        displayData();
        setId();
        MetierView.getSelectionModel().clearSelection();
    }

    private void dialog(Alert.AlertType alertType, String s) {
        Alert alert = new Alert(alertType, s);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Info");
        alert.showAndWait();
    }

    private void clear() {
        metierID.setText(null);
        ddm.setValue(null);
        libelle.clear();
        this.codeStatut = "0";
    }

    private void setId() {
        DomaineDeMetierModel domaineDeMetierModel = new DomaineDeMetierModel();
        metierID.setText(domaineDeMetierModel.getId());
    }

    private void displayData() {
        listDDM = crudData.getAll();
        System.out.println(listDDM);
        MetierView.setItems(listDDM);
    }
}


