package Controllers;

import Implementation.ImplClient;
import Implementation.ImplDomaineDeMetier;
import Implementation.ImplMetier;
import Interfaces.CRUDInterface;
import Models.ClientModel;
import Models.DomaineDeMetierModel;
import Models.MetierModel;
import Models.PartenaireModel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;

import java.io.Serializable;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class ClientController implements Initializable, Serializable {

    private String codeStatut;

    ObservableList<ClientModel> listClient;

    CRUDInterface<ClientModel> crudData = new ImplClient();


    @FXML
    private TableView<ClientModel> clientView;

    @FXML
    private TableColumn<ClientModel, String> idColumn;

    @FXML
    private TableColumn<ClientModel, String> nomOrganisationColumn;

    @FXML
    private TableColumn<ClientModel, String> telephoneColumn;

    @FXML
    private TableColumn<ClientModel, String> emailColumn;

    @FXML
    private Pane clientPane;

    @FXML
    private Label topLeftLabel;

    @FXML
    private Label clientID;

    @FXML
    private JFXTextField nom_organisation;

    @FXML
    private JFXTextField telephone;

    @FXML
    private JFXTextField email;

    @FXML
    private JFXTextField numero_siret;

    @FXML
    private JFXTextField adresse;

    @FXML
    private JFXTextField code_postal;

    @FXML
    private JFXTextField nom_ville;

    @FXML
    private JFXTextField nom_pays;

    @FXML
    private JFXButton reset;

    @FXML
    private JFXButton save;

    @FXML
    private JFXButton deleteButton;

    @FXML
    void clicTableData(MouseEvent event) {
        codeStatut = "1";
        try {
            ClientModel clic = clientView.getSelectionModel().getSelectedItems().get(0);
            clientID.setText(clic.getId());
            nom_organisation.setText(clic.getNom_organisation());
            telephone.setText(clic.getTelephone());
            email.setText(clic.getEmail());
            numero_siret.setText(clic.getNumero_siret());
            adresse.setText(clic.getLigne_adresse());
            nom_pays.setText(clic.getNom_pays());
            nom_ville.setText(clic.getNom_ville());
            code_postal.setText(clic.getCode_postal());
        } catch (Exception e) {
        }

    }

    @FXML
    void delete(ActionEvent event) {
        ClientModel clic = clientView.getSelectionModel().getSelectedItems().get(0);
        crudData.delete(clic.getId());
        displayData();
        clear();
    }

    @FXML
    void reset(ActionEvent event) {
        clear();
    }

    @FXML
    void save(ActionEvent event) {
        ClientModel newClient = new ClientModel();
        newClient.setNom_organisation(nom_organisation.getText());
        newClient.setEmail(email.getText());
        newClient.setTelephone(telephone.getText());
        newClient.setCode_postal(code_postal.getText());
        newClient.setLigne_adresse(adresse.getText());
        newClient.setNom_pays(nom_pays.getText());
        newClient.setNom_ville(nom_ville.getText());
        newClient.setNumero_siret(numero_siret.getText());
        if (clientID.getText() != null) {
            newClient.setId(clientID.getText());
        }
        if (codeStatut.equals("0")) {
            crudData.insert(newClient);
            dialog(Alert.AlertType.INFORMATION, "Donnée ajoutée");
        } else {
            crudData.update(newClient);
            dialog(Alert.AlertType.INFORMATION, "Donnée modifiée");
        }
        displayData();
        clear();
        setId();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<ClientModel, String> cellData) ->
                        cellData.getValue().idProperty());
        nomOrganisationColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<ClientModel, String> cellData) ->
                        cellData.getValue().nom_organisationProperty());
        telephoneColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<ClientModel, String> cellData) ->
                        cellData.getValue().telephoneProperty());
        emailColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<ClientModel, String> cellData) ->
                        cellData.getValue().emailProperty());
        codeStatut = "0";
        displayData();
        setId();
        clientView.getSelectionModel().clearSelection();
    }

    private void dialog(Alert.AlertType alertType, String s) {
        Alert alert = new Alert(alertType, s);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Info");
        alert.showAndWait();
    }

    private void clear() {
        clientID.setText(null);
        nom_organisation.clear();
        telephone.clear();
        email.clear();
        numero_siret.clear();
        adresse.clear();
        code_postal.clear();
        nom_ville.clear();
        nom_pays.clear();
        this.codeStatut = "0";
    }

    private void setId() {
        DomaineDeMetierModel domaineDeMetierModel = new DomaineDeMetierModel();
        clientID.setText(domaineDeMetierModel.getId());
    }

    private void displayData() {
        listClient = crudData.getAll();
        clientView.setItems(listClient);
    }
}
