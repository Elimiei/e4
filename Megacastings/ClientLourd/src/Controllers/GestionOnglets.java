package Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class GestionOnglets implements Initializable {

    @FXML
    private Pane centerPane;

    @FXML
    private JFXButton buttonOffre, buttonPartenaire, buttonMetier, buttonDDM, buttonTDC, buttonClient;

    @FXML
    void handleTab(ActionEvent event) {

        if (event.getSource() == buttonOffre) {

            try {
                Pane offrePane = FXMLLoader.load(getClass().getResource("/Resources/offre.fxml"));
                if(centerPane.getChildren().size() != 0){
                    centerPane.getChildren().remove(0);
                }
                centerPane.getChildren().add(offrePane);
            }
            catch(IOException e) {
                e.printStackTrace();
            }

        } else if (event.getSource() == buttonPartenaire) {

            try {
                Pane partenairePane = FXMLLoader.load(getClass().getResource("/Resources/partenaire.fxml"));
                if(centerPane.getChildren().size()!= 0){
                    centerPane.getChildren().remove(0);
                }
                centerPane.getChildren().add(partenairePane);
            }
            catch(IOException e) {
                e.printStackTrace();
            }


        } else if (event.getSource() == buttonMetier) {

            try {
                Pane metierPane = FXMLLoader.load(getClass().getResource("/Resources/metier.fxml"));
                if(centerPane.getChildren().size() != 0){
                    centerPane.getChildren().remove(0);
                }
                centerPane.getChildren().add(metierPane);
            }
            catch(IOException e) {
                e.printStackTrace();
            }

        } else if (event.getSource() == buttonDDM) {

            try {
                Pane ddmPane = FXMLLoader.load(getClass().getResource("/Resources/domaine_de_metier.fxml"));
                if(centerPane.getChildren().size() != 0){
                    centerPane.getChildren().remove(0);
                }
                centerPane.getChildren().add(ddmPane);
            }
            catch(IOException e) {
                e.printStackTrace();
            }

        } else if (event.getSource() == buttonTDC) {
            try {
                Pane tdcPane = FXMLLoader.load(getClass().getResource("/Resources/typedecontrat.fxml"));
                if(centerPane.getChildren().size() != 0){
                    centerPane.getChildren().remove(0);
                }
                centerPane.getChildren().add(tdcPane);
            }
            catch(IOException e) {
                e.printStackTrace();
            }

        } else if (event.getSource() == buttonClient) {
            try {
                Pane clientPane = FXMLLoader.load(getClass().getResource("/Resources/client.fxml"));
                if(centerPane.getChildren().size() != 0){
                    centerPane.getChildren().remove(0);
                }
                centerPane.getChildren().add(clientPane);
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Pane offrePane = FXMLLoader.load(getClass().getResource("/Resources/offre.fxml"));
            if(centerPane.getChildren().size() != 0){
                centerPane.getChildren().remove(0);
            }
            centerPane.getChildren().add(offrePane);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }
}
