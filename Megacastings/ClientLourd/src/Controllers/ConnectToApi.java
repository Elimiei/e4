package Controllers;

import Models.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ConnectToApi {

    public static final String url = "http://172.16.0.20:8080/api/";
    // public static final String url = "http://localhost:8080/";


    // METHODES GENERIQUES ------------------------------------------------------------------------------------------

    public String getAll(String type) {
        String outputFinal = "";
        try {
            URL url = new URL(this.url + type);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                outputFinal = (output);
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputFinal;
    }

    public String getOneById(String id, String type) {
        String outputFinal = "";
        try {
            URL url = new URL(this.url + type + "/" + id);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                outputFinal = (output);
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputFinal;
    }

    public void deleteOne(String type, String id) {
        try {
            URL url = new URL(this.url + type + "/" + id);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            System.out.println(con.getResponseCode());

        } catch (IOException e) {

        }
    }

    //METHODES SPECIFIQUES ----------------------------------------------------------------------------------------------

    public void createOnePartenaire(PartenaireModel object) {

        try {
            URL url = new URL(this.url + "partenaires");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"nom_organisation\":\"" + object.getNom_organisation() +
                            "\",\"email\":\"" + object.getEmail() +
                            "\", \"date_partenariat\": \"" + object.getFormatDate() +
                            "\",\"mot_de_passe\":\"" + object.getMot_de_passe() + "\"}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }

    public void updateOnePartenaire(PartenaireModel object) {
        try {
            URL url = new URL(this.url + "partenaires/" + object.getId());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"nom_organisation\":\"" + object.getNom_organisation() +
                            "\",\"email\":\"" + object.getEmail() +
                            "\",\"id\":\"" + object.getId() +
                            "\", \"date_partenariat\": \"" + object.getFormatDate() +
                            "\",\"mot_de_passe\":\"" + object.getMot_de_passe() + "\"}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }

    public void createOneMetier(MetierModel object) {

        try {
            URL url = new URL(this.url + "metiers");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"libelle\":\"" + object.getLibelle() +
                            "\",\"domaineDeMetier\":{\"id\":" + object.getDomaine_de_metier().getId() + ",\"libelle\":\"" + object.getDomaine_de_metier().getLibelle() + "\"}}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }
        } catch (IOException e) {
        }
    }

    public void updateOneMetier(MetierModel object) {
        try {
            URL url = new URL(this.url + "metiers/" + object.getId());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"libelle\":\"" + object.getLibelle() +
                            "\",\"domaineDeMetier\":{\"id\":" + object.getDomaine_de_metier().getId() + ",\"libelle\":\"" + object.getDomaine_de_metier().getLibelle() + "\"}" +
                            ",\"id\":\"" + object.getId() + "\"}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }

    public void createOneDDM(DomaineDeMetierModel object) {

        try {
            URL url = new URL(this.url + "domainesdemetier");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"libelle\":\"" + object.getLibelle() + "\"}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }

    public void updateOneDDM(DomaineDeMetierModel object) {
        try {
            URL url = new URL(this.url + "domainesdemetier/" + object.getId());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"libelle\":\"" + object.getLibelle() +
                            "\",\"id\":\"" + object.getId() + "\"}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }

    public void createOneClient(ClientModel object) {

        try {
            URL url = new URL(this.url + "clients");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"nom_organisation\":\"" + object.getNom_organisation() +
                            "\",\"email\":\"" + object.getEmail() +
                            "\", \"telephone\": \"" + object.getTelephone() +
                            "\", \"numero_siret\": \"" + object.getNumero_siret() +
                            "\", \"ligne_adresse\": \"" + object.getLigne_adresse() +
                            "\", \"code_postal\": \"" + object.getCode_postal() +
                            "\", \"nom_ville\": \"" + object.getNom_ville() +
                            "\", \"nom_pays\": \"" + object.getNom_pays() + "\"}";
            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }


    public void updateOneClient(ClientModel object) {
        try {
            URL url = new URL(this.url + "clients/" + object.getId());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"nom_organisation\":\"" + object.getNom_organisation() +
                            "\",\"email\":\"" + object.getEmail() +
                            "\", \"telephone\": \"" + object.getTelephone() +
                            "\", \"numero_siret\": \"" + object.getNumero_siret() +
                            "\", \"ligne_adresse\": \"" + object.getLigne_adresse() +
                            "\", \"code_postal\": \"" + object.getCode_postal() +
                            "\", \"nom_ville\": \"" + object.getNom_ville() +
                            "\", \"id\": \"" + object.getId() +
                            "\", \"nom_pays\": \"" + object.getNom_pays() + "\"}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }

    public boolean isExistingName(String name) {
        boolean res = false;
        try {
            URL url = new URL(this.url + "utilisateurs/find/" + name);
//            172.16.0.20:8080/api/utilisateurs/find/
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            System.out.println("Output from Server .... \n");
            if ((br.readLine()) != null) {
                res = true;
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public boolean isExistingPassword(String password) {
        boolean res = false;
        try {
            URL url = new URL(this.url + "utilisateurs/password/" + password);
//            172.16.0.20:8080/api/utilisateurs/find/
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            System.out.println("Output from Server .... \n");
            if ((br.readLine()) != null) {
                res = true;
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public void createOneOffre(OffreModel object) {

        try {
            // URL url = new URL("http://172.16.0.20:8080/api/offres");
            URL url = new URL(this.url + "offres");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"libelle\":\"" + object.getLibelle() +
                            "\",\"date_publication\":\"" + object.getFormatDatePublication() +
                            "\", \"duree_publication\": \"" + object.getDuree_publication() +
                            "\", \"date_debut_contrat\": \"" + object.getFormatDateDebContrat() +
                            "\", \"nombre_poste\": \"" + object.getNombre_poste() +
                            "\", \"description_poste\": \"" + object.getDescription_poste() +
                            "\", \"description_profil\": \"" + object.getDescription_profil() +
                            "\",\"client\":{\"id\":" + object.getClient().getId() + ",\"libelle\":\"" + object.getClient().getNom_organisation() + "\""
                            + ",\"telephone\":" + object.getClient().getTelephone() + ",\"email\":\"" + object.getClient().getEmail() + "\""
                            + ",\"numero_siret\":" + object.getClient().getNumero_siret() + ",\"ligne_adresse\":\"" + object.getClient().getLigne_adresse() + "\""
                            + ",\"nom_ville\":\"" + object.getClient().getNom_ville() + "\"" + ",\"nom_pays\":\"" + object.getClient().getNom_pays()
                            + "\"}" +
                            ",\"metier\":{\"id\":" + object.getMetier().getId() + ",\"libelle\":\"" + object.getMetier().getLibelle() +
                            "\",\"domaineDeMetier\":{\"id\":" + object.getMetier().getDomaine_de_metier().getId() + ",\"libelle\":\"" + object.getMetier().getDomaine_de_metier().getLibelle() + "\"}}" +
                            ",\"typeDeContrat\":{\"id\":" + object.getTypeDeContrat().getId() + ",\"libelle\":\"" + object.getTypeDeContrat().getLibelle() + "\"" +
                            ",\"code\":\"" + object.getTypeDeContrat().getCode() + "\"}"
                            + "}";
            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }

    public void updateOneOffre(OffreModel object) {
        try {
            URL url = new URL(this.url + "offres/" + object.getId());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"libelle\":\"" + object.getLibelle() +
                            "\",\"date_publication\":\"" + object.getFormatDatePublication() +
                            "\", \"duree_publication\": \"" + object.getDuree_publication() +
                            "\", \"date_debut_contrat\": \"" + object.getFormatDateDebContrat() +
                            "\", \"nombre_poste\": \"" + object.getNombre_poste() +
                            "\", \"description_poste\": \"" + object.getDescription_poste() +
                            "\", \"description_profil\": \"" + object.getDescription_profil() +
                            "\",\"client\":{\"id\":" + object.getClient().getId() + ",\"libelle\":\"" + object.getClient().getNom_organisation() + "\""
                            + ",\"telephone\":" + object.getClient().getTelephone() + ",\"email\":\"" + object.getClient().getEmail() + "\""
                            + ",\"numero_siret\":" + object.getClient().getNumero_siret() + ",\"ligne_adresse\":\"" + object.getClient().getLigne_adresse() + "\""
                            + ",\"nom_ville\":\"" + object.getClient().getNom_ville() + "\"" + ",\"nom_pays\":\"" + object.getClient().getNom_pays()
                            + "\"}" +
                            ",\"metier\":{\"id\":" + object.getMetier().getId() + ",\"libelle\":\"" + object.getMetier().getLibelle() +
                            "\",\"domaineDeMetier\":{\"id\":" + object.getMetier().getDomaine_de_metier().getId() + ",\"libelle\":\"" + object.getMetier().getDomaine_de_metier().getLibelle() + "\"}}" +
                            ",\"typeDeContrat\":{\"id\":" + object.getTypeDeContrat().getId() + ",\"libelle\":\"" + object.getTypeDeContrat().getLibelle() + "\"" +
                            ",\"code\":\"" + object.getTypeDeContrat().getCode() + "\"}"
                            +
                            ", \"id\": \"" + object.getId() + "\"}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }

    public void createOneTDC(TypeDeContratModel object) {

        try {
            URL url = new URL(this.url + "typesdecontrat");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"libelle\":\"" + object.getLibelle() +
                            "\",\"code\":\"" + object.getCode() + "\"}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }

    public void updateOneTDC(TypeDeContratModel object) {
        try {
            URL url = new URL(this.url + "typesdecontrat/" + object.getId());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            String jsonInputString =
                    "{\"libelle\":\"" + object.getLibelle() +
                            "\",\"code\":\"" + object.getCode() +
                            "\",\"id\":\"" + object.getId() + "\"}";

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }

        } catch (IOException e) {
        }
    }
}

