package Models;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class PartenaireModel {

    @JsonProperty("id")
    public StringProperty id = new SimpleStringProperty();

    @JsonProperty("nom_organisation")
    public StringProperty nom_organisation = new SimpleStringProperty();

    @JsonProperty("date_partenariat")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public ObjectProperty<java.util.Date> date_partenariat = new SimpleObjectProperty<>();

    @JsonProperty
    public StringProperty email = new SimpleStringProperty();

    @JsonProperty
    public StringProperty mot_de_passe = new SimpleStringProperty();
    public String formatDate;

    public PartenaireModel() {
    }


    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getNom_organisation() {
        return nom_organisation.get();
    }

    public StringProperty nom_organisationProperty() {
        return nom_organisation;
    }

    public void setNom_organisation(String nom_organisation) {
        this.nom_organisation.set(nom_organisation);
    }

    public Date getDate_partenariat() {
        return date_partenariat.get();
    }

    public ObjectProperty<Date> date_partenariatProperty() {
        return date_partenariat;
    }

    public void setDate_partenariat(Date date_partenariat) {
        this.date_partenariat.set(date_partenariat);
    }

    public String getFormatDate() {
        Date date = getDate_partenariat();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = df.format(date);
        return format;
    }

    public void setFormatDate(String formatDate) {
        this.formatDate = formatDate;
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getMot_de_passe() {
        return mot_de_passe.get();
    }

    public StringProperty mot_de_passeProperty() {
        return mot_de_passe;
    }

    public void setMot_de_passe(String mot_de_passe) {
        this.mot_de_passe.set(mot_de_passe);
    }

    @Override
    public String toString() {
        return "PartenaireModel{" +
                "id=" + id +
                ", nom_organisation=" + nom_organisation +
                ", date_partenariat=" + date_partenariat +
                ", email=" + email +
                ", mot_de_passe=" + mot_de_passe +
                ", formatDate='" + formatDate + '\'' +
                '}';
    }
}
