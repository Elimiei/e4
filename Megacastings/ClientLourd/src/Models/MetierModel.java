package Models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MetierModel {
    @JsonProperty
    public StringProperty id = new SimpleStringProperty();

    @JsonProperty
    public StringProperty libelle = new SimpleStringProperty();

    public DomaineDeMetierModel getDomaine_de_metier() {
        return domaine_de_metier;
    }

    public void setDomaine_de_metier(DomaineDeMetierModel domaine_de_metier) {
        this.domaine_de_metier = domaine_de_metier;
    }

    @JsonProperty("domaineDeMetier")
    public DomaineDeMetierModel domaine_de_metier;

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getLibelle() {
        return libelle.get();
    }

    public StringProperty libelleProperty() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle.set(libelle);
    }

    @Override
    public String toString() {
        return this.getLibelle();
    }
}
