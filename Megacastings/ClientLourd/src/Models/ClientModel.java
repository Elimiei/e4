package Models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ClientModel {
    @JsonProperty
    public StringProperty id = new SimpleStringProperty();

    @JsonProperty
    public StringProperty nom_organisation = new SimpleStringProperty();

    @JsonProperty
    public StringProperty telephone = new SimpleStringProperty();

    @JsonProperty
    public StringProperty email = new SimpleStringProperty();

    @JsonProperty
    public StringProperty numero_siret = new SimpleStringProperty();

    @JsonProperty
    public StringProperty ligne_adresse = new SimpleStringProperty();

    @JsonProperty
    public StringProperty code_postal = new SimpleStringProperty();

    @JsonProperty
    public StringProperty nom_ville = new SimpleStringProperty();

    @JsonProperty
    public StringProperty nom_pays = new SimpleStringProperty();

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getNom_organisation() {
        return nom_organisation.get();
    }

    public StringProperty nom_organisationProperty() {
        return nom_organisation;
    }

    public void setNom_organisation(String nom_organisation) {
        this.nom_organisation.set(nom_organisation);
    }

    public String getTelephone() {
        return telephone.get();
    }

    public StringProperty telephoneProperty() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone.set(telephone);
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getNumero_siret() {
        return numero_siret.get();
    }

    public StringProperty numero_siretProperty() {
        return numero_siret;
    }

    public void setNumero_siret(String numero_siret) {
        this.numero_siret.set(numero_siret);
    }

    public String getLigne_adresse() {
        return ligne_adresse.get();
    }

    public StringProperty ligne_adresseProperty() {
        return ligne_adresse;
    }

    public void setLigne_adresse(String ligne_adresse) {
        this.ligne_adresse.set(ligne_adresse);
    }

    public String getCode_postal() {
        return code_postal.get();
    }

    public StringProperty code_postalProperty() {
        return code_postal;
    }

    public void setCode_postal(String code_postal) {
        this.code_postal.set(code_postal);
    }

    public String getNom_ville() {
        return nom_ville.get();
    }

    public StringProperty nom_villeProperty() {
        return nom_ville;
    }

    public void setNom_ville(String nom_ville) {
        this.nom_ville.set(nom_ville);
    }

    public String getNom_pays() {
        return nom_pays.get();
    }

    public StringProperty nom_paysProperty() {
        return nom_pays;
    }

    public void setNom_pays(String nom_pays) {
        this.nom_pays.set(nom_pays);
    }

    @Override
    public String toString() {
        return this.getNom_organisation();
    }
}
