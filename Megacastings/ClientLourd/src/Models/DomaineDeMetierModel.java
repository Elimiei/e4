package Models;

import Interfaces.CRUDInterface;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class DomaineDeMetierModel {

    @JsonProperty
    public StringProperty id = new SimpleStringProperty();

    @JsonProperty
    public StringProperty libelle = new SimpleStringProperty();

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getLibelle() {
        return libelle.get();
    }

    public StringProperty libelleProperty() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle.set(libelle);
    }

    @Override
    public String toString() {
        return this.getLibelle();
    }

    public String toStringFull() {
        return "DomaineDeMetierModel{" +
                "id=" + id +
                ", libelle=" + libelle +
                '}';
    }
}
