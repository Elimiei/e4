package Models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UtilisateurModel {
    @JsonProperty("id")
    public StringProperty id = new SimpleStringProperty();

    @JsonProperty("identifiant")
    public StringProperty identifiant = new SimpleStringProperty();

    @JsonProperty("mot_de_passe")
    public StringProperty mot_de_passe = new SimpleStringProperty();

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getIdentifiant() {
        return identifiant.get();
    }

    public StringProperty identifiantProperty() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant.set(identifiant);
    }

    public String getMot_de_passe() {
        return mot_de_passe.get();
    }

    public StringProperty mot_de_passeProperty() {
        return mot_de_passe;
    }

    public void setMot_de_passe(String mot_de_passe) {
        this.mot_de_passe.set(mot_de_passe);
    }


}
