package Models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OffreModel {
    @JsonProperty
    public StringProperty id = new SimpleStringProperty();

    @JsonProperty
    public StringProperty libelle = new SimpleStringProperty();

    @JsonProperty("date_publication")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public ObjectProperty<Date> date_publication = new SimpleObjectProperty<>();

    @JsonProperty
    public StringProperty duree_publication = new SimpleStringProperty();

    @JsonProperty("date_debut_contrat")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public ObjectProperty<Date> date_debut_contrat = new SimpleObjectProperty<>();

    @JsonProperty
    public StringProperty nombre_poste = new SimpleStringProperty();

    @JsonProperty
    public StringProperty description_poste = new SimpleStringProperty();

    @JsonProperty
    public StringProperty description_profil = new SimpleStringProperty();

    @JsonProperty("client")
    public ClientModel client;

    @JsonProperty("metier")
    public MetierModel metier;

    @JsonProperty("typeDeContrat")
    public TypeDeContratModel typeDeContrat;

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getLibelle() {
        return libelle.get();
    }

    public StringProperty libelleProperty() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle.set(libelle);
    }

    public Date getDate_publication() {
        return date_publication.get();
    }

    public ObjectProperty<Date> date_publicationProperty() {
        return date_publication;
    }

    public void setDate_publication(Date date_publication) {
        this.date_publication.set(date_publication);
    }

    public String getDuree_publication() {
        return duree_publication.get();
    }

    public StringProperty duree_publicationProperty() {
        return duree_publication;
    }

    public void setDuree_publication(String duree_publication) {
        this.duree_publication.set(duree_publication);
    }

    public Date getDate_debut_contrat() {
        return date_debut_contrat.get();
    }

    public ObjectProperty<Date> date_debut_contratProperty() {
        return date_debut_contrat;
    }

    public void setDate_debut_contrat(Date date_debut_contrat) {
        this.date_debut_contrat.set(date_debut_contrat);
    }

    public String getFormatDateDebContrat() {
        Date date = getDate_debut_contrat();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = df.format(date);
        return format;
    }

    public String getFormatDatePublication() {
        Date date = getDate_publication();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = df.format(date);
        return format;
    }

    public String getNombre_poste() {
        return nombre_poste.get();
    }

    public StringProperty nombre_posteProperty() {
        return nombre_poste;
    }

    public void setNombre_poste(String nombre_poste) {
        this.nombre_poste.set(nombre_poste);
    }

    public String getDescription_poste() {
        return description_poste.get();
    }

    public StringProperty description_posteProperty() {
        return description_poste;
    }

    public void setDescription_poste(String description_poste) {
        this.description_poste.set(description_poste);
    }

    public String getDescription_profil() {
        return description_profil.get();
    }

    public StringProperty description_profilProperty() {
        return description_profil;
    }

    public void setDescription_profil(String description_profil) {
        this.description_profil.set(description_profil);
    }

    public ClientModel getClient() {
        return client;
    }

    public void setClient(ClientModel client) {
        this.client = client;
    }

    public MetierModel getMetier() {
        return metier;
    }

    public void setMetier(MetierModel metier) {
        this.metier = metier;
    }

    public TypeDeContratModel getTypeDeContrat() {
        return typeDeContrat;
    }

    public void setTypeDeContrat(TypeDeContratModel typeDeContrat) {
        this.typeDeContrat = typeDeContrat;
    }
}
