package Interfaces;

import Models.PartenaireModel;
import javafx.collections.ObservableList;

public interface CRUDInterface<T> {
    void insert(T object);
    void delete(String id);
    void update(T object);
    ObservableList<T> getAll();
    T getById(String id);
}
