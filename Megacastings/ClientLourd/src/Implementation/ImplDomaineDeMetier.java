package Implementation;

import Controllers.Connect;
import Controllers.ConnectToApi;
import Interfaces.CRUDInterface;
import Models.DomaineDeMetierModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class ImplDomaineDeMetier implements CRUDInterface<DomaineDeMetierModel> {


    @Override
    public void insert(DomaineDeMetierModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.createOneDDM(object);
    }

    @Override
    public void delete(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.deleteOne("domainesdemetier", id);
    }

    @Override
    public void update(DomaineDeMetierModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.updateOneDDM(object);
    }

    @Override
    public ObservableList<DomaineDeMetierModel> getAll() {

        ConnectToApi cta = new ConnectToApi();
        cta.getAll("domainesdemetier");

        ObjectMapper mapper = new ObjectMapper();

        ArrayList<DomaineDeMetierModel> listDDM = new ArrayList<>();

        try {
            TypeReference<List<DomaineDeMetierModel>> typeReference = new TypeReference<List<DomaineDeMetierModel>>() {
            };
            listDDM = mapper.readValue(cta.getAll("domainesdemetier"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }

        ObservableList<DomaineDeMetierModel> oList = FXCollections.observableArrayList(listDDM);
        return oList;
    }

    @Override
    public DomaineDeMetierModel getById(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.getOneById(id,"domainesdemetier");

        ObjectMapper mapper = new ObjectMapper();

        DomaineDeMetierModel newDDM = new DomaineDeMetierModel();

        try {
            TypeReference<DomaineDeMetierModel> typeReference = new TypeReference<DomaineDeMetierModel>() {
            };
            newDDM = mapper.readValue(cta.getOneById(id,"domainesdemetier"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }
        return newDDM;
    }
}
