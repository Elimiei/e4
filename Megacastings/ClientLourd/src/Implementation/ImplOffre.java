package Implementation;

import Controllers.ConnectToApi;
import Interfaces.CRUDInterface;
import Models.OffreModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class ImplOffre implements CRUDInterface<OffreModel> {
    @Override
    public void insert(OffreModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.createOneOffre(object);
    }

    @Override
    public void delete(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.deleteOne("offres", id);
    }

    @Override
    public void update(OffreModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.updateOneOffre(object);
    }

    @Override
    public ObservableList<OffreModel> getAll() {
        ConnectToApi cta = new ConnectToApi();
        cta.getAll("offres");

        ObjectMapper mapper = new ObjectMapper();

        ArrayList<OffreModel> listOffres = new ArrayList<>();

        try {
            TypeReference<List<OffreModel>> typeReference = new TypeReference<List<OffreModel>>() {
            };
            listOffres = mapper.readValue(cta.getAll("offres"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }

        ObservableList<OffreModel> oList = FXCollections.observableArrayList(listOffres);
        return oList;
    }

    @Override
    public OffreModel getById(String id) {
        return null;
    }
}
