package Implementation;

import Controllers.ConnectToApi;
import Interfaces.CRUDInterface;
import Models.ClientModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class ImplClient implements CRUDInterface<ClientModel> {
    @Override
    public void insert(ClientModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.createOneClient(object);
    }

    @Override
    public void delete(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.deleteOne("clients", id);
    }

    @Override
    public void update(ClientModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.updateOneClient(object);
    }

    @Override
    public ObservableList<ClientModel> getAll() {

        ConnectToApi cta = new ConnectToApi();
        cta.getAll("clients");

        ObjectMapper mapper = new ObjectMapper();
        ArrayList<ClientModel> listClients = new ArrayList<>();

        try {
            TypeReference<List<ClientModel>> typeReference = new TypeReference<List<ClientModel>>() {
            };
            listClients = mapper.readValue(cta.getAll("clients"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }

        ObservableList<ClientModel> oList = FXCollections.observableArrayList(listClients);

        return oList;
    }

    @Override
    public ClientModel getById(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.getOneById(id,"clients");

        ObjectMapper mapper = new ObjectMapper();

        ClientModel newClient = new ClientModel();

        try {
            TypeReference<ClientModel> typeReference = new TypeReference<ClientModel>() {
            };
            newClient = mapper.readValue(cta.getOneById(id,"clients"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }
        return newClient;
    }
}
