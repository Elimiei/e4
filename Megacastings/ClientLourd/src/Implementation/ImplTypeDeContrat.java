package Implementation;

import Controllers.ConnectToApi;
import Models.TypeDeContratModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import Interfaces.CRUDInterface;


import java.util.ArrayList;
import java.util.List;

public class ImplTypeDeContrat implements CRUDInterface<TypeDeContratModel> {

    @Override
    public void insert(TypeDeContratModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.createOneTDC(object);
    }

    @Override
    public void delete(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.deleteOne("typesdecontrat", id);
    }

    @Override
    public void update(TypeDeContratModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.updateOneTDC(object);
    }

    @Override
    public ObservableList<TypeDeContratModel> getAll() {
        ConnectToApi cta = new ConnectToApi();
        cta.getAll("typesdecontrat");

        ObjectMapper mapper = new ObjectMapper();

        ArrayList<TypeDeContratModel> listTDC = new ArrayList<>();

        try {
            TypeReference<List<TypeDeContratModel>> typeReference = new TypeReference<List<TypeDeContratModel>>() {
            };
            listTDC = mapper.readValue(cta.getAll("typesdecontrat"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }

        ObservableList<TypeDeContratModel> oList = FXCollections.observableArrayList(listTDC);
        return oList;
    }

    @Override
    public TypeDeContratModel getById(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.getOneById(id,"typesdecontrat");

        ObjectMapper mapper = new ObjectMapper();

        TypeDeContratModel newTDC = new TypeDeContratModel();

        try {
            TypeReference<TypeDeContratModel> typeReference = new TypeReference<>() {
            };
            newTDC = mapper.readValue(cta.getOneById(id,"typesdecontrat"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }
        return newTDC;
    }
}
