package Implementation;

import Controllers.ConnectToApi;
import Interfaces.CRUDInterface;
import Models.MetierModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class ImplMetier implements CRUDInterface<MetierModel> {

    @Override
    public void insert(MetierModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.createOneMetier(object);
    }

    @Override
    public void delete(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.deleteOne("metiers", id);
    }

    @Override
    public void update(MetierModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.updateOneMetier(object);
    }

    @Override
    public ObservableList<MetierModel> getAll() {

        ConnectToApi cta = new ConnectToApi();
        cta.getAll("metiers");

        ObjectMapper mapper = new ObjectMapper();

        ArrayList<MetierModel> listMetiers = new ArrayList<>();

        try {
            TypeReference<List<MetierModel>> typeReference = new TypeReference<List<MetierModel>>() {
            };
            listMetiers = mapper.readValue(cta.getAll("metiers"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }

        ObservableList<MetierModel> oList = FXCollections.observableArrayList(listMetiers);
        return oList;
    }

    @Override
    public MetierModel getById(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.getOneById(id,"metiers");

        ObjectMapper mapper = new ObjectMapper();

        MetierModel newMetier = new MetierModel();

        try {
            TypeReference<MetierModel> typeReference = new TypeReference<MetierModel>() {
            };
            newMetier = mapper.readValue(cta.getOneById(id,"metiers"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }
        return newMetier;
    }
}
