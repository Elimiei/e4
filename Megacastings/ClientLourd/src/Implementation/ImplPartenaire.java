package Implementation;

import Controllers.Connect;
import Controllers.ConnectToApi;
import Interfaces.CRUDInterface;
import Models.PartenaireModel;
import com.fasterxml.jackson.core.type.TypeReference;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ImplPartenaire implements CRUDInterface<PartenaireModel> {

    @Override
    public void insert(PartenaireModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.createOnePartenaire(object);
    }

    @Override
    public void delete(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.deleteOne("partenaires", id);
    }

    @Override
    public void update(PartenaireModel object) {
        ConnectToApi cta = new ConnectToApi();
        cta.updateOnePartenaire(object);
    }

    @Override
    public ObservableList<PartenaireModel> getAll() {
        ConnectToApi cta = new ConnectToApi();
        cta.getAll("partenaires");

        ObjectMapper mapper = new ObjectMapper();
        ArrayList<PartenaireModel> listPartenaires = new ArrayList<>();

        try {
            TypeReference<List<PartenaireModel>> typeReference = new TypeReference<>() {
            };
            listPartenaires = mapper.readValue(cta.getAll("partenaires"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }

        ObservableList<PartenaireModel> oList = FXCollections.observableArrayList(listPartenaires);

        return oList;
    }

    @Override
    public PartenaireModel getById(String id) {
        ConnectToApi cta = new ConnectToApi();
        cta.getOneById(id,"partenaires");

        ObjectMapper mapper = new ObjectMapper();

        PartenaireModel newPartenaire = new PartenaireModel();

        try {
            TypeReference<PartenaireModel> typeReference = new TypeReference<>() {
            };
            newPartenaire = mapper.readValue(cta.getOneById(id,"partenaires"), typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("non réussi");
        }
        return newPartenaire;
    }

    public void connectWithoutApi() {

        Connect c = new Connect();
        ObservableList<PartenaireModel> listPartenaires = FXCollections.observableArrayList();
        try {
            String sql = "select * from Partenaire";
            ResultSet rs = c.getConnection().createStatement().executeQuery(sql);
            while (rs.next()) {
                PartenaireModel m = new PartenaireModel();
                int nb = rs.getInt(1);
                m.setId(Integer.toString(nb));
                m.setNom_organisation(rs.getString(2));
                m.setDate_partenariat(rs.getDate(3));
                m.setEmail(rs.getString(4));
                m.setMot_de_passe(rs.getString(5));
                listPartenaires.add(m);
            }
        } catch (Exception ex) {
            Logger.getLogger(ImplPartenaire.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
