package Implementation;


import Controllers.ConnectToApi;

public class ImplUtilisateur {
    public boolean isExistingUser(String name, String password) {
        boolean res = true;

        ConnectToApi cta = new ConnectToApi();

        res = cta.isExistingName(name) && cta.isExistingPassword(password);

        return res;
    }
}
