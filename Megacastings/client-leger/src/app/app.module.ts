import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {AppComponent} from './app.component';
import {ConseilsComponent} from './Conseils/conseils.component';
import {FichesComponent} from './Fiches/fiches.component';
import {InterviewsComponent} from './Interviews/interviews.component';
import {OffresComponent} from './Offres/offres.component';
import {EventManager} from './Shared/eventManager.component';
import {OffresService} from './Offres/offres.service';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {AccordionModule, ButtonModule, DialogModule, DropdownModule, InputTextModule, PanelModule} from 'primeng/primeng';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {DataViewModule} from 'primeng/dataview';
import {InfosPratiquesComponent} from './Infos-pratiques/infos-pratiques.component';
import {MatDividerModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    OffresComponent,
    FichesComponent,
    ConseilsComponent,
    InterviewsComponent,
    InfosPratiquesComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AutoCompleteModule,
    AccordionModule,
    BrowserAnimationsModule,
    DataViewModule,
    PanelModule,
    InputTextModule,
    DialogModule,
    ButtonModule,
    DropdownModule,
    MatDividerModule
  ],
  providers: [
    EventManager,
    OffresService
  ],
  bootstrap: [AppComponent],

})
export class AppModule { }
