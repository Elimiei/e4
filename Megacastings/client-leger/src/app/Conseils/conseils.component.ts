import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {OffresComponent} from '../Offres/offres.component';
import {FichesComponent} from '../Fiches/fiches.component';
import {InterviewsComponent} from '../Interviews/interviews.component';
import {Subscription} from 'rxjs/internal/Subscription';
import {EventManager} from '../Shared/eventManager.component';
import {TOGGLE_INFOS, TOGGLE_OFFRES, TOGGLE_FICHES, TOGGLE_INTERVIEWS, TOGGLE_CONSEILS} from '../megacastings.constants';
import {AccordionModule} from 'primeng/accordion';


@Component({
  selector: 'conseils',
  templateUrl: './conseils.component.html',
  styleUrls: ['./../app.component.css']
})

export class ConseilsComponent implements OnInit {


  eventSubscriber: Subscription;

  constructor(private eventManager: EventManager) {
  }

  ngOnInit() {
  }

}
