import {Component, OnDestroy, OnInit} from '@angular/core';
import {TOGGLE_OFFRES, TOGGLE_FICHES, TOGGLE_INTERVIEWS, TOGGLE_CONSEILS,
TOGGLE_APROPOS, TOGGLE_PACKS, TOGGLE_FLUX, TOGGLE_AIDE, TOGGLE_CONTACT } from './megacastings.constants';
import {Subscription} from 'rxjs/internal/Subscription';
import {EventManager} from './Shared/eventManager.component';
import {Partenaire} from './Models/partenaire.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'
  ]
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'client-leger';
  eventSubscriber: Subscription;
  fichesFull = false;
  interviewsFull = false;
  conseilsFull = false;
  offresFull = true;
  aproposFull = false;
  packsFull = false;
  fluxFull = false;
  aideFull = false;
  contactFull = false;

  constructor(private eventManager: EventManager) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.eventSubscriber.unsubscribe();
  }

  // Offres

  toggleOffres() {
    this.offresFull = true;
    this.fichesFull = false;
    this.interviewsFull = false;
    this.conseilsFull = false;
    this.aproposFull = false;
    this.packsFull = false;
    this.fluxFull = false;
    this.aideFull = false;
    this.contactFull = false;
    this.eventManager.broadcast({ name: TOGGLE_OFFRES });
  }

  // Infos pratiques

  toggleFiches() {
    this.fichesFull = true;
    this.interviewsFull = false;
    this.conseilsFull = false;
    this.offresFull = false;
    this.aproposFull = false;
    this.packsFull = false;
    this.fluxFull = false;
    this.aideFull = false;
    this.contactFull = false;
    this.eventManager.broadcast({ name: TOGGLE_FICHES });
  }

  toggleInterviews() {
    this.fichesFull = false;
    this.interviewsFull = true;
    this.conseilsFull = false;
    this.offresFull = false;
    this.aproposFull = false;
    this.packsFull = false;
    this.fluxFull = false;
    this.aideFull = false;
    this.contactFull = false;
    this.eventManager.broadcast({ name: TOGGLE_INTERVIEWS });
  }

  toggleConseils() {
    this.fichesFull = false;
    this.interviewsFull = false;
    this.conseilsFull = true;
    this.offresFull = false;
    this.aproposFull = false;
    this.packsFull = false;
    this.fluxFull = false;
    this.aideFull = false;
    this.contactFull = false;
    this.eventManager.broadcast({ name: TOGGLE_CONSEILS });
  }

  // footer

  toggleApropos() {
    this.fichesFull = false;
    this.interviewsFull = false;
    this.conseilsFull = false;
    this.offresFull = false;
    this.aproposFull = true;
    this.packsFull = false;
    this.fluxFull = false;
    this.aideFull = false;
    this.contactFull = false;
    this.eventManager.broadcast({ name: TOGGLE_APROPOS });
  }

  togglePacks() {
    this.fichesFull = false;
    this.interviewsFull = false;
    this.conseilsFull = false;
    this.offresFull = false;
    this.aproposFull = false;
    this.packsFull = true;
    this.fluxFull = false;
    this.aideFull = false;
    this.contactFull = false;
    this.eventManager.broadcast({ name: TOGGLE_PACKS });
  }

  toggleFlux() {
    this.fichesFull = false;
    this.interviewsFull = false;
    this.conseilsFull = false;
    this.offresFull = false;
    this.aproposFull = false;
    this.packsFull = false;
    this.fluxFull = true;
    this.aideFull = false;
    this.contactFull = false;
    this.eventManager.broadcast({ name: TOGGLE_FLUX });
  }

  toggleAide() {
    this.fichesFull = false;
    this.interviewsFull = false;
    this.conseilsFull = false;
    this.offresFull = false;
    this.aproposFull = false;
    this.packsFull = false;
    this.fluxFull = false;
    this.aideFull = true;
    this.contactFull = false;
    this.eventManager.broadcast({ name: TOGGLE_AIDE });
  }

  toggleContact() {
    this.fichesFull = false;
    this.interviewsFull = false;
    this.conseilsFull = false;
    this.offresFull = false;
    this.aproposFull = false;
    this.packsFull = false;
    this.fluxFull = false;
    this.aideFull = false;
    this.contactFull = true;
    this.eventManager.broadcast({ name: TOGGLE_CONTACT });
  }



}
