import {Route} from '@angular/router';

import {AppComponent} from './app.component';

export const HOME_ROUTE: Route = {
  path: '',
  component: AppComponent,
  data: {
    authorities: [],
    pageTitle: 'home.title'
  }
};
