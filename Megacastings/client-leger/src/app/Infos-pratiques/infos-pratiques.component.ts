import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {OffresComponent} from '../Offres/offres.component';
import {Subscription} from 'rxjs/internal/Subscription';
import {EventManager} from '../Shared/eventManager.component';
import {TOGGLE_INFOS, TOGGLE_OFFRES, TOGGLE_FICHES, TOGGLE_INTERVIEWS, TOGGLE_CONSEILS} from '../megacastings.constants';
import {AccordionModule} from 'primeng/accordion';


@Component({
  selector: 'infos-pratiques',
  templateUrl: './infos-pratiques.component.html',
  styleUrls: ['./../app.component.css']
})
export class InfosPratiquesComponent implements OnInit {

  fichesFull = false;
  interviewsFull = false;
  conseilsFull = false;

  eventSubscriber: Subscription;

  constructor(private eventManager: EventManager) {
  }

  ngOnInit() {
    this.eventSubscriber = this.eventManager.subscribe(TOGGLE_FICHES, (response) => this.afficherFiches());
    this.eventSubscriber = this.eventManager.subscribe(TOGGLE_INTERVIEWS, (response) => this.afficherInterviews());
    this.eventSubscriber = this.eventManager.subscribe(TOGGLE_CONSEILS, (response) => this.afficherConseils());
  }

  afficherFiches() {
    this.fichesFull = true;
    this.interviewsFull = false;
    this.conseilsFull = false;
  }

  afficherInterviews() {
    this.interviewsFull = true;
    this.fichesFull = false;
    this.conseilsFull = false;
  }

  afficherConseils() {
    this.conseilsFull = true;
    this.interviewsFull = false;
    this.fichesFull = false;
  }

}
