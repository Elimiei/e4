import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {Offre} from '../Models/offres.model';
import {Partenaire} from '../Models/partenaire.model';

@Injectable()

export class OffresService  {
  constructor(private http: HttpClient ) {}

  getAll(): Observable<Offre[]> {
    return this.http
      .get<Offre[]>('http://172.16.0.20:8080/api/offres/');
    // .get<Offre[]>('http://localhost:8080/offres/');
  }

  getFlux(email: string, password: string) {
    const partenaire = new Partenaire();
    partenaire.email = 'rgrgrgr';
    partenaire.mot_de_passe = '1234';
    return this.http
      .post<Partenaire>('http://localhost:8080/offres/flux', partenaire);
    console.log('ok');
  }
}
