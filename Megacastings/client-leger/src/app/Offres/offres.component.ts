import {Component, OnInit} from '@angular/core';
import {EventManager} from '../Shared/eventManager.component';
import {Offre} from '../Models/offres.model';
import {OffresService} from './offres.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {DomaineDeMetier, Metier} from '../Models/metier.model';
import {Partenaire} from '../Models/partenaire.model';


@Component({
  selector: 'offres',
  templateUrl: './offres.component.html',
  styleUrls: ['./../app.component.css']
})
export class OffresComponent implements OnInit {
  offres: Offre[];
  isLoading = false;
  displayDialog: boolean;
  selectedOffre: Offre;
  metier = new Metier;
  domaineDeMetier = new DomaineDeMetier();
  sortField: string;
  sortOrder: number;

  constructor(
    private eventManager: EventManager,
    public offreService: OffresService  ) {
  }

  ngOnInit() {
    this.loadOffres();
  }

  loadOffres() {
    this.isLoading = true;
    this.offreService.getAll().subscribe(
      (res: any[]) => {
        this.offres = res;
        this.offres = this.offres.filter(offre => this.check(offre));
        this.isLoading = false;
      },
      (res: HttpErrorResponse) => {
        this.isLoading = false;
        console.log('Erreur lors de la récupération des données');
      }
    );
  }


  check(offre) {
    let res;
    const datePubli = new Date(offre.date_publication);
    const finDate = new Date(offre.date_publication);
    const today = new Date();
    finDate.setDate(datePubli.getDate() + offre.duree_publication);

    if (finDate < today) {
      res = false;
    } else {
      res = true;
    }
    return res;
  }

  selectOffre(event: Event, offre: Offre) {
    this.selectedOffre = offre;
    this.isLoading = true;
    this.displayDialog = true;
    this.isLoading = false;
    event.preventDefault();
  }

  onDialogHide() {
    this.selectedOffre = null;
  }
}
