export class Client {
  constructor(
    public id?: number,
    public code_postal?: string,
    public email?: string,
    public ligne_adresse?: string,
    public nom_organisation?: string,
    public nom_pays?: string,
    public nom_ville?: string,
    public numero_siret?: string,
    public telephone?: string,
    ) {}

}
