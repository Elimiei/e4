export class Metier  {
  constructor(
    public id?: number,
    public libelle?: string,
    public domaineDeMetier?: DomaineDeMetier
  ) {
  }
}

export class DomaineDeMetier {
  constructor(
  public id?: number,
  public libelle?: string
  ) {
  }
}
