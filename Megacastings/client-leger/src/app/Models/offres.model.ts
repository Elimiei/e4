import {DomaineDeMetier, Metier} from './metier.model';
import {TypeDeContrat} from './contrat.model';
import {Client} from './client.model';

export class Offre  {
  constructor(
    public id?: number,
    public libelle?: string,
    public date_publication?: Date,
    public date_debut_contrat?: Date,
    public duree_publication?: number,
    public nombre_poste?: string,
    public description_poste?: string,
    public description_profil?: string,
    public id_Metier?: number,
    public metier?: Metier,
    public client?: Client,
    public typeDeContrat?: TypeDeContrat
  ) {
  }
}
