import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { OffresComponent } from '../Offres/offres.component';
import { ConseilsComponent } from '../Conseils/conseils.component';
import { FichesComponent } from '../Fiches/fiches.component';
import { Subscription } from 'rxjs/internal/Subscription';
import { EventManager } from '../Shared/eventManager.component';
import { TOGGLE_INFOS, TOGGLE_OFFRES, TOGGLE_FICHES, TOGGLE_INTERVIEWS, TOGGLE_CONSEILS } from '../megacastings.constants';
import { AccordionModule } from 'primeng/accordion';


@Component({
  selector: 'interviews',
  templateUrl: './interviews.component.html',
  styleUrls: ['./../app.component.css']
})
export class InterviewsComponent implements OnInit {

  eventSubscriber: Subscription;

  constructor(private eventManager: EventManager) {
  }

  ngOnInit() {
  }

}
